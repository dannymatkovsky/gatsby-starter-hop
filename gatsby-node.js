/* global require */
/* global exports */

const path = require('path')
const graphqlSchema = require('./graphql-schema')

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions

  createTypes(graphqlSchema)
}

exports.createPages = ({ graphql, actions: { createPage } }) => {
  return graphql(`
    query initialQuery {
      allStrapiPage {
        nodes {
          slug
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      throw result.errors
    }

    // Create pages
    const pages = result.data.allStrapiPage.nodes
    pages.map(({ slug }) => {
      createPage({
        path: `/page/${slug}`,
        component: path.resolve('./src/pages/_page.js'),
        context: {
          slug
        }
      })
    })
  })
}
