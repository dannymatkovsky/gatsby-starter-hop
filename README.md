# HOp Gatsby starter

Highly opinionated Gatsby starter use Strapi as a backend and CSS Modules for styling.

## Local installation

1. `yarn install`.
2. `yarn devellop`.

## Deploy

[https://www.gatsbyjs.org/docs/deploying-and-hosting/](https://www.gatsbyjs.org/docs/deploying-and-hosting/)
