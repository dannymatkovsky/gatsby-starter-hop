const separator = `
`
const schemas = []
// Common definitions
schemas.push(`
type Seo {
  title: String,
  description: String,
  image: File @link(from: "image___NODE")
}
type StrapiSiteSettings implements Node @dontInfer {
  seo: Seo
}
`)

// Main page
schemas.push(`
type StrapiMainPage implements Node @dontInfer {
  seo: Seo
}
`)

// Menus
schemas.push(`
  type MenuLink {
    id: String
    title: String
    url: String
  }
  type StrapiMenu implements Node @dontInfer {
    links: [MenuLink]
  }
`)

// Pages
schemas.push(`
type StrapiPage implements Node @dontInfer {
  id: String
  title: String
  slug: String
  text: String
  image: File @link(from: "image___NODE")
  seo: Seo
}
`)

module.exports = schemas.join(separator)
