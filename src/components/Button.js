import React from 'react'
import PropTypes from 'prop-types'
import styles from './Button.module.scss'

function Button({ children, type, className, ...props }) {
  const normalizedClassName = `${styles.root}${
    className ? ` ${className}` : ``
  }`

  return (
    <button className={normalizedClassName} type={type || 'button'} {...props}>
      {children}
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.node.isRequired
}

export default Button
