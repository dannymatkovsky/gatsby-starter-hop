import React from "react"
import HtmlToReact, { Parser } from "html-to-react"
import Link from "./Link"
import styles from "./HtmlContent.module.scss"

const processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React)

const processingInstructions = [
  {
    // Custom image `src` processing
    shouldProcessNode(node) {
      return node.nodeType === 1 && node.name === "img"
    },
    processNode(node) {
      const src = node.attribs.src || ""
      return React.createElement(
        "img",
        {
          ...node.attribs,
          src:
            src.startsWith("http://") || src.startsWith("https://")
              ? src
              : `${process.env.GATSBY_IMAGE_BASE_URL}/${src.replace(
                  /^\/+/,
                  ""
                )}`,
        }
        // processNodeDefinitions.processDefaultNode(node)
      )
    },
  },
  {
    // Links
    shouldProcessNode(node) {
      return node.nodeType === 1 && node.name === "a"
    },
    processNode(node, children) {
      const { href: to, ...props } = node.attribs
      return React.createElement(
        Link,
        {
          to,
          ...props,
        },
        children
      )
    },
  },
  {
    // Anything else
    shouldProcessNode() {
      return true
    },
    processNode: processNodeDefinitions.processDefaultNode,
  },
]

function isValidNode(node) {
  return node.type !== "script"
}

function HtmlContent({ content }) {
  const htmlToReactParser = new Parser()
  const reactElement = htmlToReactParser.parseWithInstructions(
    content,
    isValidNode,
    processingInstructions
  )
  return <div className={styles.content}>{reactElement}</div>
}

export default HtmlContent
