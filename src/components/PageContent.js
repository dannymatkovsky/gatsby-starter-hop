import React from 'react'
import PropTypes from 'prop-types'
import HtmlContent from './HtmlContent'
import styles from './PageContent.module.scss'

function PageContent({ content }) {
  return (
    <>
      <div className={styles.root}>
        <div className={styles.header}>
          <h1 className={styles.title}>{content.title}</h1>
        </div>
        <div className={styles.container}>
          <HtmlContent content={content.text} />
        </div>
      </div>
    </>
  )
}

PageContent.propTypes = {
  content: PropTypes.object.isRequired
}

export default PageContent
