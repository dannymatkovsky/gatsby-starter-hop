import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'

function SEO({ title, description, image, meta }) {
  const { allSiteSettings } = useStaticQuery(graphql`
    query SeoSettingsQuery {
      allSiteSettings: strapiSiteSettings {
        seo {
          title
          description
          image {
            childImageSharp {
              resize(width: 1200) {
                src
              }
            }
          }
        }
      }
    }
  `)
  const siteSettings = allSiteSettings.seo || {}

  return (
    <Helmet
      htmlAttributes={{ lang: 'en' }}
      title={title}
      titleTemplate={`${siteSettings.title} | %s`}
      defaultTitle={siteSettings.title}
      meta={[
        {
          name: `description`,
          content: description || siteSettings.description
        },
        {
          property: `og:title`,
          content: title
        },
        {
          property: `og:description`,
          content: description || siteSettings.description
        },
        {
          property: `og:type`,
          content: 'article'
        },
        {
          property: `og:image`,
          content: image?.src || siteSettings?.image?.src
        },
        {
          name: `twitter:card`,
          content: `summary`
        }
      ].concat(meta || [])}
    />
  )
}

SEO.proptypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.object,
  meta: PropTypes.array
}
export default SEO
