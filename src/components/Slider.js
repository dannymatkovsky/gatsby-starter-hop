import React, { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import Button from './Button'
import styles from './Slider.module.scss'

function Slider({ slides, step }) {
  const containerRef = useRef(null)
  const sliderRef = useRef(null)
  const [offset, setOffset] = useState(0)

  const moveSliderLeft = () => {
    const container = containerRef.current
    const slider = sliderRef.current
    setOffset(offset =>
      slider.offsetWidth + offset <= container.offsetWidth
        ? offset
        : offset - step
    )
  }

  const moveSliderRight = () => {
    setOffset(offset => (offset === 0 ? offset : offset + step))
  }

  return (
    <div className={styles.root} ref={containerRef}>
      <Button className={styles.button} onClick={moveSliderRight}>
        &lt;
      </Button>
      <div
        className={styles.slider}
        ref={sliderRef}
        style={{ transform: `translate(${offset}px)` }}
      >
        {slides}
      </div>
      <Button className={styles.button} onClick={moveSliderLeft}>
        &gt;
      </Button>
    </div>
  )
}

Slider.propTypes = {
  children: PropTypes.node.isRequired
}

export default Slider
