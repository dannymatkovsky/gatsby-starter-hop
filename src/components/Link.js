import React from 'react'
import PropTypes from 'prop-types'
import { Link as GatsbyLink } from 'gatsby'
import { AnchorLink } from 'gatsby-plugin-anchor-links'

function Link({ to, children, ...props }) {
  const isAbsolute = !!(to.startsWith('http://') || to.startsWith('https://'))
  const isAnchor = !!(!isAbsolute && to.includes('#'))

  return isAbsolute ? (
    <a href={to} target="_blank" rel="noreferrer" {...props}>
      {children}
    </a>
  ) : isAnchor ? (
    <AnchorLink to={to} {...props}>
      {children}
    </AnchorLink>
  ) : (
    <GatsbyLink to={to} {...props}>
      {children}
    </GatsbyLink>
  )
}

Link.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
}

export default Link
