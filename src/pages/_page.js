import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../layouts/main'
import SEO from '../components/Seo'
import Content from '../components/PageContent'

function Article({
  data: {
    strapiPage: { seo, ...page }
  }
}) {
  const seoSettings = { title: page.title, image: page.seoImage, ...seo }

  return (
    <Layout>
      <SEO {...seoSettings} />
      <Content content={page} />
    </Layout>
  )
}

const query = graphql`
  query pageQuery($slug: String!) {
    strapiPage(slug: { eq: $slug }) {
      title
      text
      image {
        childImageSharp {
          fluid(maxWidth: 1920) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
      seoImage: image {
        childImageSharp {
          resize(width: 1200) {
            src
          }
        }
      }
      seo {
        title
        description
        image {
          childImageSharp {
            resize(width: 1200) {
              src
            }
          }
        }
      }
    }
  }
`

export default Article
export { query }
