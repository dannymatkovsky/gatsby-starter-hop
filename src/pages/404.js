import React from 'react'
import Layout from '../layouts/main'
import SEO from '../components/Seo'

function NotFound() {
  return (
    <Layout>
      <SEO title="404: Not Found" />
      <h1>404: Not Found</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Layout>
  )
}

export default NotFound
