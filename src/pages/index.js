import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../layouts/main'
import SEO from '../components/Seo'

function Home({ data: { mainPage } }) {
  return (
    <Layout>
      <SEO {...(mainPage.seo || {})} />
      This is main page
    </Layout>
  )
}

const query = graphql`
  query mainPageQuery {
    mainPage: strapiMainPage {
      seo {
        title
        description
        image {
          childImageSharp {
            fixed(width: 1200) {
              ...GatsbyImageSharpFixed_noBase64
            }
          }
        }
      }
    }
  }
`

export default Home
export { query }
