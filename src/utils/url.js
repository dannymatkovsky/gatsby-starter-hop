import { globalHistory as history } from '@reach/router'

function isActiveLink(linkPath) {
  const linkParts = linkPath.split('/')
  const currentParts = history.location.pathname.split('/')
  return linkParts.every((item, index) => item === currentParts[index])
}

export { isActiveLink }
