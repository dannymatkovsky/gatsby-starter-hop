import React from 'react'
import Header from './main/components/Header'
import Footer from './main/components/Footer'
import MainContext from './main/contexts/Main'
import styles from './main.module.scss'

function Main({ children, ...rest }) {
  return (
    <div className={styles.layout}>
      <MainContext.Provider value={rest}>
        <Header />

        <main>{children}</main>

        <Footer />
      </MainContext.Provider>
    </div>
  )
}

export default Main
