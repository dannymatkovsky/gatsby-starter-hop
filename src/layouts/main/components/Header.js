import React, { useState } from 'react'
import Link from '../../../components/Link'
import MainMenu from './MainMenu'
import MobileMenu from './MobileMenu'
import styles from './Header.module.scss'

function header() {
  const [mobileMenuOpened, setMobileMenuOpened] = useState(false)

  function toggleMobileMenu() {
    setMobileMenuOpened(!mobileMenuOpened)
  }

  return (
    <>
      <header className={styles.header}>
        <div className={styles.container}>
          <Link to="/" className={styles.logo} alt="" />
          <MainMenu />
          <button
            type="button"
            className={styles.mobileMenuToggler}
            aria-label="menu"
            onClick={toggleMobileMenu}
          >
            menu on mobile
          </button>
        </div>
      </header>
      <MobileMenu open={mobileMenuOpened} onMenuItemClick={toggleMobileMenu} />
    </>
  )
}

export default header
