import React from 'react'
import PropTypes from 'prop-types'
import Link from '../../../components/Link'
import styles from './MobileMenu.module.scss'
import { isActiveLink } from '../../../utils/url'

function MobileMenu({ open, onMenuItemClick }) {
  return (
    <div className={`${styles.root}`}>
      <Link
        to="/"
        onClick={onMenuItemClick}
        className={
          isActiveLink('/projects')
            ? `${styles.item} ${styles.itemActive}`
            : `${styles.item}`
        }
      >
        Link
      </Link>
    </div>
  )
}

MobileMenu.propTypes = {
  open: PropTypes.bool.isRequired,
  onMenuItemClick: PropTypes.func.isRequired
}

export default MobileMenu
