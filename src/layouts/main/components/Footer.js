import React from 'react'
import styles from './Footer.module.scss'

function Footer() {
  return (
    <>
      <footer className={styles.footer}>
        <div className={styles.container}>
          <a href="/" className={styles.logo}></a>
          <span className={styles.copyright}>
            © Studena-IT, 2019-{new Date().getFullYear()}
          </span>
        </div>
      </footer>
    </>
  )
}

export default Footer
