import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import Link from '../../../components/Link'
import { isActiveLink } from '../../../utils/url'
import styles from './Menu.module.scss'

function mainMenu() {
  const {
    strapiMenu: { links = [] }
  } = useStaticQuery(graphql`
    query menuQuery {
      strapiMenu {
        links {
          id
          title
          url
        }
      }
    }
  `)

  return (
    <div className={styles.root}>
      {links.map(({ id, url, title }) => (
        <Link
          to={url}
          className={
            isActiveLink(url)
              ? `${styles.item} ${styles.itemActive}`
              : `${styles.item}`
          }
          key={id}
        >
          {title}
        </Link>
      ))}
    </div>
  )
}

export default mainMenu
