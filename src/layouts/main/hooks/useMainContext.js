import { useContext } from 'react'
import MainContext from '../contexts/Main'

// You can use context in layout components to get any prop passed from page components
// e.g. const {isMainPage} = useMainContext()
const useMainContext = () => useContext(MainContext)

export default useMainContext
