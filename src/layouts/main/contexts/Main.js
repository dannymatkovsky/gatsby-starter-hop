import { createContext } from 'react'

// You can use context to pass any prop from page components to layout components
// e.g. <Layout isMainPage> ... </Layout>
const MainContext = createContext()

export default MainContext
